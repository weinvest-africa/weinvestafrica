<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'PagesController@index')->name('index');

Auth::routes();

Route::post('/investments/invest/{id}', function (Request $request, $id) {
    try {
        \Mail::send(new \App\Mail\ProjectMail($request, $id, "reply"));
    } catch (Exception $err) {
    }
    return redirect('/')->with('toast_success', 'Thanks! Email sent to owner!');
})->name('invest');

Route::get('/investments', 'PagesController@investments')->name('investments');
Route::get('/investments/search', 'PagesController@search')->name('search');
Route::get('/investments/filter', 'PagesController@searchFilter')->name('filter');
Route::get('/investments/category/{category}', 'PagesController@searchCategory')->name('category');
Route::get('/investments/cities/{id}', 'PagesController@getCities')->name('cities');
Route::get('/investments/{id}', 'PagesController@details')->name('investments.show');
Route::get('/services', 'PagesController@services')->name('services');
Route::get('/services/details/{name}', 'PagesController@servicesDetails')->name('services-details');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/postproject', 'PagesController@postproject')->name('post');
Route::get('/help/show-more', 'PagesController@showMore')->name('help.showMore');
Route::get('/blog/{id}', 'PagesController@showblog')->name('blog.showblog');
Route::get('/help/searchblog', 'PagesController@searchblog')->name("searchblog");
Route::get('/help', 'PagesController@help')->name('help');
Route::get('/help/contact', 'PagesController@contact')->name('contact');
Route::get('/blog', 'PagesController@blog')->name('blog');
Route::post('/help/subscribe', 'MailChimpController@subscribe')->name('subscribe');

Route::resource('categories', 'CategoryController');
Route::resource('projects', 'ProjectController');
Route::get('/projects/image/{filename}', 'ProjectController@renderImage')->name('projects.image');
Route::post('/projects/store', 'ProjectController@store')->name('projects.store');

Route::get('blogs/image/{filename}', 'Admin\BlogController@renderImage')->name('posts.image');
Route::get('team/image/{filename}', 'Admin\TeamController@renderImage')->name('users.image');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('login', 'Admin\Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Admin\Auth\LoginController@login')->name('login');

    Route::group(['middleware' => 'admin'], function () {
        Route::get('/', function () {
            return redirect()->route('admin.dashboard');
        });
        Route::get('dashboard', 'Admin\DashboardController@getDashboard')->name('dashboard');

        Route::resource('categories', 'Admin\CategoryController');

        Route::resource('countries', 'Admin\CountryController');
        Route::resource('team', 'Admin\TeamController');
        Route::resource('cities', 'Admin\CityController');
        Route::resource('faqs', 'Admin\FAQController');
        Route::PATCH('categories/{category}/restore', 'Admin\CategoryController@restore')->name('categories.restore');
        Route::PATCH('countries/{country}/restore', 'Admin\CountryController@restore')->name('countries.restore');

        Route::resource('blogs', 'Admin\BlogController');

        Route::resource('projects', 'Admin\ProjectController');
        Route::any('projects/{id}/{appraisal}', function ($id, $appraisal) {
            try {
                \Mail::send(new \App\Mail\ProjectMail(null, $id, $appraisal));
            } catch (Exception $err) {
            }
            return redirect('admin');
        })->name('appraisalMail');

        // PROFILE
        Route::get('/profile', 'Admin\ProfileController@getProfile')->name('profile');
        Route::post('/profile/edit', 'Admin\ProfileController@editProfile')->name('profile.edit');
        Route::post('/settings/edit', 'Admin\ProfileController@editSiteSettings')->name('settings.edit');
        Route::post('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');

        Route::get('logout', 'Admin\Auth\LoginController@logout')->name('logout');
    });
});