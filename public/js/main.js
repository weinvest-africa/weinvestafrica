$("#accordion").on("hide.bs.collapse show.bs.collapse", (e) => {
    $(e.target).prev().find("i").toggleClass("fa-angle-up fa-angle-down");
});

$("#accordion").on("click", () => {
    $("i.fa").css("margin", "-0.1em 0");
});

$(document).ready(function () {
    // Add smooth scrolling to all links
    $("a.investbtn").on("click", function (event) {
        event.preventDefault();
        $("html, body").animate(
            {
                scrollTop: $($(this).attr("href")).offset().top,
            },
            500
        );
    });

    $("#categorieslist li").on("click", function (e) {
        e.preventDefault();
        window.location = $(this).data("href");
    });
});

$(window).scroll(function () {
    if ($(document).scrollTop() > 50) {
        $("div#header2").removeAttr("id");
        $("div.page-header").attr("id", "header3");
        $("#header").css("visibility", "hidden");
    } else {
        $("div#header3").removeAttr("id");
        $("div.page-header").attr("id", "header2");
        $("#header").css("visibility", "visible");
    }
});

var slider1 = new Slider("input.span1", {
    orientation: "horizontal",
    min: 3,
    max: 13,
    value: [
        parseInt($("#roi-range > div >label.lower").text()),
        parseInt($("#roi-range > div >label.upper").text()),
    ],
    range: true,
    tooltip: "always",
});

var slider2 = new Slider("input.span2", {
    orientation: "horizontal",
    min: 1,
    max: 12,
    value: [
        parseInt($("#duration-range  > div > label.lower").text()),
        parseInt($("#duration-range  > div > label.upper").text()),
    ],
    range: true,
    tooltip: "always",
});

var sliderValue1;
var sliderValue2;

slider1.on("slideStart", () => {
    sliderValue1 = slider1.getValue();
    $("#roi-range > div >label.lower").text(sliderValue1[0] + " %");
    $("#roi-range > div >label.upper").text(sliderValue1[1] + " %");
    console.log("Slider Value: ", sliderValue1);
});
slider1.on("slideStop", () => {
    sliderValue1 = slider1.getValue();
    $("#roi-range > div > label.lower").text(sliderValue1[0] + " %");
    $("#roi-range > div > label.upper").text(sliderValue1[1] + " %");
    console.log("Slider Value: ", sliderValue1);
});

slider2.on("slideStart", () => {
    sliderValue2 = slider2.getValue();
    $("#duration-range  > div > label.lower").text(sliderValue2[0] + " Months");
    $("#duration-range  > div > label.upper").text(sliderValue2[1] + " Months");
    console.log("Slider Value: ", sliderValue2);
});
slider2.on("slideStop", () => {
    sliderValue2 = slider2.getValue();
    $("#duration-range  > div > label.lower").text(sliderValue2[0] + " Months");
    $("#duration-range  > div > label.upper").text(sliderValue2[1] + " Months");
    console.log("Slider Value: ", sliderValue2);
});

/*  ==========================================
    SHOW UPLOADED IMAGE
* ========================================== */
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#imageResult").attr("src", e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $("#upload").on("change", function () {
        readURL(input);
    });
});

/*  ==========================================
    SHOW UPLOADED IMAGE NAME
* ========================================== */
var input = document.getElementById("upload");
var infoArea = document.getElementById("upload-label");

input.addEventListener("change", showFileName);

function showFileName(event) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea.textContent = "File name: " + fileName;
}
