<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Blog extends Model
{
    public function getImageURL()
    {
        return $this->post_image ? route('posts.image', ['filename' => $this->post_image])
        : asset('images/projects/no-project-image.png');
    }

    public function getShortTitle()
    {
        return Str::limit($this->title, 20, '...');
    }

    public function getShortDesc()
    {
        return Str::limit($this->description, 400, '...');
    }
}