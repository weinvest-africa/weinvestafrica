<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Member extends Model
{
    public function getImageURL()
    {
        return $this->picture ? route('users.image', ['filename' => $this->picture]) : asset('images/projects/no-project-image.png');
    }

    public function getShortDesc()
    {
        return Str::limit($this->description, 60, '...');
    }
}