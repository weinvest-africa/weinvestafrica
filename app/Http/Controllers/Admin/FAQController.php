<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Faq;
use Illuminate\Support\Facades\Validator;

class FAQController extends Controller
{
    protected $search = [
        'voided' => false
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $this->search;
        $faqs = new faq();

        $faqs = $faqs->where('voided', false);

        if ($request->get('voided')) {
            $search['voided'] = $request->get('voided');
            $faqs = $faqs->orWhere('voided', true);
        }

        $faqs = $faqs->orderBy('created_at', 'desc')->get();
        return view('admin.faqs.index', compact('faqs', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'question' => 'required|unique:faqs,question',
            'answer' => 'nullable'
        ]);

        $faq = new faq();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();

        return redirect()->route('admin.faqs.index')
            ->with('success', 'FAQ created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = faq::findOrFail($id);
        return view('admin.faqs.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faq = faq::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'question' => 'required',
            'answer' => 'nullable',
        ]);

        // Check to see if another faq already exist with the same name but different id
        $validator->after(function ($validator) use ($request, $faq) {
            $existing_cat = faq::where('question', $request->name)->first();
            if ($existing_cat && $existing_cat->id != $faq->id) {
                $validator->errors()->add('question', __('This question already exists'));
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();

        return redirect()->route('admin.faqs.index')
            ->with('success', 'FAQ updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::findOrFail($id);
        $faq->voided = true;
        $faq->save();

        return redirect()->route('admin.faqs.index')->with('success', 'FAQ deleted successfully');
    }

    public function restore($id)
    {
        $faq = Faq::findOrFail($id);
        $faq->voided = false;
        $faq->save();

        return redirect()->route('admin.faqs.index')->with('success', 'FAQ restored successfully');
    }
}