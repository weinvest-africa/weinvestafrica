<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    protected $search = [
        'voided' => false
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $this->search;
        $countries = new Country();

        $countries = $countries->where('voided', false);

        if ($request->get('voided')) {
            $search['voided'] = $request->get('voided');
            $countries = $countries->orWhere('voided', true);
        }

        $countries = $countries->orderBy('name', 'ASC')->get();
        return view('admin.countries.index', compact('countries', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|unique:countries,name',
        ]);

        $country = new Country();
        $country->name = $request->name;
        $country->save();

        return redirect()->route('admin.countries.index')
            ->with('success', 'Country created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('admin.countries.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $country = Country::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        // Check to see if another country already exist with the same name but different id
        $validator->after(function ($validator) use ($request, $country) {
            $existing_ctry = Country::where('name', $request->name)->first();
            if ($existing_ctry && $existing_ctry->id != $country->id) {
                $validator->errors()->add('name', __('This name has already been taken'));
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $country->name = $request->name;
        $country->save();

        return redirect()->route('admin.countries.index')
            ->with('success', 'Country updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        $country->voided = true;
        $country->save();

        return redirect()->route('admin.countries.index')->with('success', 'Country deleted successfully');
    }

    public function restore($id)
    {
        $country = Country::findOrFail($id);
        $country->voided = false;
        $country->save();

        return redirect()->route('admin.countries.index')->with('success', 'Country restored successfully');
    }
}
