<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function getProfile()
    {
        $user = Auth('admin')->user();
        $setting = Setting::all();
        return view('admin.profile', compact('user', 'setting'));
    }

    /**
     * @desc Handle Update profile update form submission
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function editProfile(Request $request)
    {
        $this->validate($request, ['name' => 'required']);

        $user = Auth('admin')->user();
        $user->name = $request['name'];
        $user->save();

        $request->session()->flash('success', __('Profile updated successfully'));
        return redirect()->back();
    }

    public function editSiteSettings(Request $request)
    {
        $this->validate($request, [
            'site_name' => 'required',
            'email' => 'required',
        ]);

        $setting = new Setting();
        $setting->site_name = $request->name;
        $setting->site_email = $request->email;
        $setting->save();

        $request->session()->flash('success', __('Site setting updated successfully'));
        return redirect()->back();
    }

    /**
     * @desc Change admin password
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        $messages = [
            'current_password.required' => __('Please enter old password'),
            'password.required' => __('Please enter password'),
        ];

        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ], $messages);

        // Ensure that user's password matches password from the form
        $validator->after(function ($validator) use ($request) {
            $current_password = Auth('admin')->user()->password;
            if (!Hash::check($request['current_password'], $current_password)) {
                $validator->errors()->add('current_password', __('Your current password does not match with the password you provided'));
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth('admin')->user();
        $user->password = Hash::make($request['password']);
        $user->update();

        $request->session()->flash('success', __('Password successfully changed'));
        return redirect()->back();

    }

}