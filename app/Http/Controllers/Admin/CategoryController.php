<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    protected $search = [
        'voided' => false
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $this->search;
        $categories = new Category();

        $categories = $categories->where('voided', false);

        if ($request->get('voided')) {
            $search['voided'] = $request->get('voided');
            $categories = $categories->orWhere('voided', true);
        }

        $categories = $categories->orderBy('name', 'ASC')->get();
        return view('admin.projects.categories.index', compact('categories', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name'        => 'required|unique:categories,name',
            'description' => 'nullable'
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();

        return redirect()->route('admin.categories.index')
            ->with('success', 'Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.projects.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'description' => 'nullable',
        ]);

        // Check to see if another category already exist with the same name but different id
        $validator->after(function ($validator) use ($request, $category) {
            $existing_cat = Category::where('name', $request->name)->first();
            if ($existing_cat && $existing_cat->id != $category->id) {
                $validator->errors()->add('name', __('This name has already been taken'));
            }
        });

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();

        return redirect()->route('admin.categories.index')
            ->with('success', 'Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->voided = true;
        $category->save();

        return redirect()->route('admin.categories.index')->with('success', 'Category deleted successfully');
    }

    public function restore($id)
    {
        $category = Category::findOrFail($id);
        $category->voided = false;
        $category->save();

        return redirect()->route('admin.categories.index')->with('success', 'Category restored successfully');
    }
}