<?php

namespace App\Http\Controllers;

use App\City;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category' => 'required|exists:categories,id',
            'investment' => 'required|numeric|min:0|max:1000000000000000',
            'projectname' => 'required',
            'email' => 'required',
            'country' => 'required',
            'address' => 'required',
            'duration' => 'required',
            'roi' => 'required',
            'description' => 'required|min:500',
            'upload' => 'image|nullable|max:1999',
            'terms' => 'required',
        ], ['terms.required' => "Please accept our terms and conditions."]);

        $project = new Project;

        $city = new City;
        $city->name = $request->input('address');

        $city->country_id = $request->input('country');

        $city->save();

        $project->category_id = $request->input('category');
        $project->name = $request->input('projectname');
        $project->email = $request->input('email');
        $project->investment = $request->input('investment');
        $project->country_id = $request->input('country');
        $project->address = $request->input('address');
        $project->duration = $request->input('duration');
        $project->roi = $request->input('roi');
        $project->description = $request->input('description');
        $project->project_image = $this->getImageInfo($request);
        $project->city_id = $city->id;

        $project->save();

        return redirect()->route('index')->with('success', "Project created successfully!\nWe'll get in touch shortly");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getImageInfo(Request $request)
    {
        // handle the file upload

        $fileNameToStore = '';
        if ($request->hasFile('upload')) {
            // get the file name with extension
            $fileNameWithExt = $request->file('upload')->getClientOriginalName();
            // get the filename
            $fileName = pathInfo($fileNameWithExt, PATHINFO_FILENAME);
            // get the extension
            $extension = $request->file('upload')->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = Str::slug($fileName . '_' . time()) . '.' . $extension;
            // upload image

            $request->file('upload')->storeAs('public/uploads/projects', $fileNameToStore);
        }

        return $fileNameToStore;
    }

    public function renderImage($filename)
    {
        $path = storage_path('app/public/uploads/projects/' . $filename);

        //if (!\File::exists($path)) abort(404);
        $file = \File::get($path);
        $type = \File::mimeType($path);
        $response = \Response::make($file, 200);
        $response->header('Content-Type', $type);
        return $response;
    }
}