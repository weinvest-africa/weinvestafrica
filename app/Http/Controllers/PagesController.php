<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\City;
use App\Country;
use App\Faq;
use App\Member;
use App\Project;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $projects = Project::where('status', 'approved')->orderBy('created_at', 'DESC')->paginate(4);
        return view('pages.index', ['projects' => $projects]);
    }

    public function investments()
    {
        $title = "Opportunities";
        $countries = Country::all();
        $projects = Project::where('status', 'approved')->orderBy('created_at', 'DESC')->paginate(6);
        $categories = Category::all();
        return view('pages.investments', compact('title', 'projects', 'countries', 'categories'));
    }

    public function services()
    {
        $title = "Services we offer";
        return view('pages.services', compact('title'));
    }

    public function about()
    {
        $title = "About";
        $members = Member::all();

        return view('pages.about', compact('title', 'members'));
    }

    public function help()
    {
        $title = "Get help";
        $faqs = Faq::paginate(4);
        $posts = Blog::orderBy('created_at', 'DESC')->get();
        $latestdates = Project::select('id', 'created_at')->where('status', 'approved')->orderBy('created_at', 'DESC')->paginate(4);
        return view('pages.help', compact('title', 'posts', 'latestdates', 'faqs'));
    }

    public function postproject()
    {
        $title = "Post your Project";
        $categories = Category::all();
        $countries = Country::all();
        return view('pages.post', compact('title', 'categories', 'countries'));
    }

    public function showblog($id)
    {
        $post = Blog::findOrFail($id);
        $posts = Blog::where('id', '!=', $id)->orderBy('created_at', 'DESC')->paginate(3);
        return view('pages.showblog', compact('post', 'posts'));
    }

    public function blog()
    {
        $posts = Blog::orderBy('updated_at', 'DESC')->paginate(5);
        $latestposts = Blog::orderBy('created_at', 'DESC')->paginate(3);
        return view('pages.blog', compact('posts', 'latestposts'));
    }

    public function searchblog(Request $request)
    {
        if ($request->has('searchterm')) {
            $searchTerm = $request->input('searchterm');
        }

        $title = "Get help";
        $faqs = Faq::paginate(4);
        $posts = Blog::where('title', 'LIKE', "%{$searchTerm}%")->orderBy('created_at', 'DESC')->get();
        $latestdates = Project::select('id', 'created_at')->where('status', 'approved')->orderBy('created_at', 'DESC')->paginate(4);

        return view('pages.help', compact('title', 'posts', 'latestdates', 'faqs'));

    }

    public function details($id)
    {
        $project = Project::find($id);
        return view('pages.details', compact('project'));
    }

    public function servicesEntrepreneurs()
    {
        return view('pages.services-entp');
    }

    public function servicesInvestors()
    {
        return view('pages.services-invst');
    }

    public function servicesDetails()
    {
        return view('pages.services-details');
    }

    public function subscribe()
    {

    }

    public function search(Request $request)
    {
        $title = "Opportunities";
        $countries = Country::all();
        $categories = Category::all();

        if ($request->has('search')) {
            $searchTerm = $request->input('search');
        }

        $projects = Project::where('status', 'approved')
            ->where('name', 'LIKE', "%{$searchTerm}%")
            ->orderBy('created_at', 'DESC')->paginate(6);

        return view('pages.investments', compact('title', 'projects', 'countries', 'categories'));
    }

    public function contact(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'comment' => 'required',
        ]);

        try {
            \Mail::send(new \App\Mail\ProjectMail($request, null, "contact"));
        } catch (Exception $err) {
        }

        return redirect()->route('help')->with('success', "Thanks for Contacting us!");
    }

    public function searchFilter(Request $request)
    {
        $title = "Opportunities";
        $countries = Country::all();
        $categories = Category::all();

        $country = $request->input('country');
        $city = $request->input('address');
        $duration = $request->input('duration');
        $roi = $request->input('roi');
        list($lowerRoi, $upperRoi) = explode(",", $roi);
        list($lowerDuration, $upperDuration) = explode(",", $duration);

        $projects = Project::where('status', 'approved')
            ->where('country_id', $country)
            ->where('city_id', $city)
            ->whereBetween('roi', [$lowerRoi, $upperRoi])
            ->whereBetween('duration', [$lowerDuration, $upperDuration])
            ->orderBy('created_at', 'DESC')->paginate(6);
        return view('pages.investments', compact('title', 'projects', 'countries', 'categories'));
    }

    public function searchCategory(Request $request, $category)
    {
        $title = "Opportunities";
        $countries = Country::all();

        $categories = Category::all();

        $category_s = Category::where('name', $category)->get()->all();

        $projects = Project::where('status', 'approved')
            ->where('category_id', $category_s[0]->id)
            ->orderBy('created_at', 'DESC')->paginate(6);

        return view('pages.investments', compact('title', 'projects', 'countries', 'categories'));
    }

    public function getCities($id)
    {
        $cities = City::where('country_id', $id)->pluck("name", "id");
        return json_encode($cities);
    }

    public function showMore(Request $request)
    {
        if ($request->ajax()) {
            $skip = $request->skip;
            $faqs = Faq::where('id', '>', $skip)->paginate(3);
            return response()->json($faqs);
        } else {
            return response()->json('Direct Access Not allowed!!');
        }
    }

}