<?php

namespace App\Mail;

use App\Project;
use App\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProjectMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $id;
    public $mailType;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $id, $mailType)
    {
        $this->data = $request;
        $this->id = $id;
        $this->mailType = $mailType;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = Setting::all()[0]->site_email;
        $adminMail = 'daniegraham97@gmail.com';
        $name = Setting::all()[0]->site_name;

        $project = Project::find($this->id);

        if ($this->mailType == "reply") {

            $subject = "An Investor for your Project";
            return $this->view('emails.mail')
                ->from($address, $name)
                ->to($project->email, $name)
                ->replyTo($address, $name)
                ->subject($subject);

        } else if ($this->mailType == "approved") {

            $subject = "Project Appraisal";
            return $this->view('emails.mail1')
                ->with('project_name', $project->name)
                ->from($address, $name)
                ->to($project->email, $name)
                ->replyTo($address, $name)
                ->subject($subject);

        } else if ($this->mailType == "rejected") {

            $subject = "Project Appraisal";
            return $this->view('emails.mail2')
                ->with('project_name', $project->name)
                ->from($address, $name)
                ->to($project->email, $name)
                ->replyTo($address, $name)
                ->subject($subject);

        } else if ($this->mailType == "contact") {

            $subject = "A question from " . $this->data->name;

            return $this->view('emails.mail3')
                ->with('data', $this->data)
                ->from($address, $name)
                ->to($adminMail, $name)
                ->replyTo($address, $name)
                ->subject($subject);
        }

    }
}