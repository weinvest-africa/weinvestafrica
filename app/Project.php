<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Project extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    // Helper function
    public function getImageURL()
    {
        return $this->project_image ? route('projects.image', ['filename' => $this->project_image])
            : asset('images/projects/no-project-image.png');
    }

    public function getShortName()
    {
        return Str::limit($this->name, 10, '...');
    }
}