
@if (session('success'))
    {{-- {{ dd() }}
    <script>
        $(function() {
            $('#successModal').modal('show');
        });
        $('#successModal').on('hidden.bs.modal', function() {
        return false;
    });
    </script> --}}
@endif

@if (session('error'))
    {{-- <script>
        $(function() {
            $('#errorModal').modal('show');
        });
        $('#errorModal').on('hidden.bs.modal', function() {
        return false;
    });
    </script> --}}
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
