<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'WeInvestInAfrica') }}</title>

    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.6/css/flag-icon.min.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.2/css/bootstrap-slider.min.css">

    <!-- Begin Mailchimp Signup Form -->
    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
        /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
        We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
    </style>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    @yield('header-style')

</head>
<body>
    <div id="app">
       <div class="fixed-top">
            {{-- Top header section --}}
        <header id="header" class="page-topline">
            <div class="container">
                <div class="row align-items-center py-4 mb-5">
                    <div class="col-12 col-sm-12 col-md-4 text-center text-md-left">
                        <span class="social-icons px-3">
                            <a href="#" class="fa fa-facebook"></a>
                            <a href="#" class="fa fa-twitter "></a>
                            <a href="#" class="fa fa-linkedin"></a>
                            <a href="#" class="fa fa-instagram"></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-8 text-center text-md-right">

                        <select class="selectpicker" data-width="fit">
                            <option data-content='<span class="flag-icon flag-icon-us mr-auto"></span> English'>English</option>
                            <option  data-content='<span class="flag-icon flag-icon-fr mr-auto"></span> Français'>Français</option>
                        </select>

                    </div>
                </div>
            </div>
        </header>

        {{-- Second header section --}}
        <div id="header2" class="py-auto page-header">
            <nav class="top-nav navbar navbar-expand-lg navbar-light ">
                <a class="navbar-brand font-weight-bold" href="{{ route('index') }}">
                    {{-- {{ config('app.name', 'WeInvestAfrica') }} --}}
                    <img src="{{ asset('images/logo_final-5.png') }}" alt="logo" class="img-fluid">
                </a>
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul id="main-menu" class="menu navbar-nav d-flex">
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="{{ route('index') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('investments*') ? 'active' : '' }}" href="{{ route('investments') }}">Investments</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('services*') ? 'active' : '' }}" href="{{ route('services') }}">Services</a>
                        </li>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('blog*') ? 'active' : '' }}" href="{{ route('blog') }}">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('about*') ? 'active' : '' }}" href="{{ route('about') }}">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('help*') ? 'active' : '' }}" href="{{ route('help') }}">Get Help</a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('post') }}" class="btn project-btn">Post your Project</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
       </div>

        <main>
            @include('inc.messages')
            @yield('content')
        </main>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Quick Links</h4>
                        <ul>
                            <li>
                                <a class="nav-link" href="{{ route('index') }}">Home</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('investments') }}">Investments</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('services') }}">Services</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('about') }}">About</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('help') }}">Get Help</a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('post') }}">Post your project</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h4>Contact us</h4>
                        <p class="text-muted">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet?</p>

                        <div class="d-flex align-items-center py-2">
                            {{-- <i class="fa fa-phone fa-lg"></i> --}}
                            <img src="{{ asset('images/phone-white.png') }}" class="img-fluid mr-2" width="20px" alt="phone">
                            <label for="contact">Call us: <span>786 789 333</span></label>
                        </div>
                        <div class="d-flex align-items-center py-2">
                            {{-- <i class="fa fa-headset fa-lg"></i> --}}
                            <img src="{{ asset('images/headset.png') }}" class="img-fluid mr-2" width="20px" alt="phone">
                            <label for="contact">Call Center: <span>786 789 555</span></label>
                        </div>
                        <div class="d-flex align-items-center py-2">
                            {{-- <i class="fa fa-envelope fa-lg"></i> --}}
                            <img src="{{ asset('images/mail.png') }}" class="img-fluid mr-2" width="20px" alt="phone">
                            <label for="contact">Skype us: <span>786 789 333</span> </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>Connect with us</h4>
                        <span class="social-icons  mt-2">
                            <a href="#" class="fa fa-facebook fa-2x"></a>
                            <a href="#" class="fa fa-twitter fa-2x"></a>
                            <a href="#" class="fa fa-linkedin fa-2x"></a>
                            <a href="#" class="fa fa-instagram fa-2x"></a>
                        </span>
                    </div>
                </div>


            </div>
            <div id="copyright" class="container-fluid">
                <div class="text-center">
                    <h6 class="mb-0 text-white font-weight-bolder">Copyright &copy; 2020 WEINVESTINAFRICA.</h6>
                </div>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    {{-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>

    {{-- Bootstrap-Slider --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.2/bootstrap-slider.min.js"></script>

    {{-- Main JS --}}
    <script src="{{ asset('js/main.js') }}"></script>

    @yield('footer-script')

    @include('sweetalert::alert')
</body>
</html>
