@extends('admin.layouts.app')
@section('title', $post->title)

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.blogs.index') }}">Blog</a></li>
                        <li class="breadcrumb-item active">{{ $post->title }}</li>
                    </ol>
                </div>
                <h4 class="page-title">Post Detail</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row flex-column-reverse flex-md-row">
        <div class="col-md-7">
            <div class="card d-block">
                <div class="card-body">
                    <!-- post title-->
                    <h3 class="mt-0 font-20">
                        {{ $post->title }}
                    </h3>

                    <h5>Post Overview:</h5>

                    <p class="mb-4 text-muted">
                        {!! $post->description !!}
                    </p>

                    <div>
                        <h5>Last Edited:</h5>
                        <p class="d-inline-block">
                            {{ $post->updated_at->diffForHumans() ?: 'N/A' }}
                        </p>
                    </div>

                </div> <!-- end card-body-->

            </div>
        </div>
        <div class="col-md-5">

            <div class="card d-block">
                <img src="{{ $post->getImageURL() }}" class="img-fluid">

            </div>
        </div>
    </div>

@endsection

@section('footer_script')
    <script></script>
@endsection
