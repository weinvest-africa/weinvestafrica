@extends('admin.layouts.app')
@section('title', 'Blog')

@section('header-style')
    <style>
        .post-card .status {
            position: absolute;
            right: 10px;
            top: 10px;
        }
    </style>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Blog</li>
                    </ol>
                </div>
                <h4 class="page-title"> Blog Posts</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex">
                    <button type="button" class="btn btn-light btn-sm mr-1 text-dark"
                            data-toggle="collapse" data-target="#collapseFilter" title="Filter results">
                        <i class="mdi mdi-filter"></i>
                    </button>
                    <div class="dropdown">
                        <button class="btn btn-light btn-sm dropdown-toggle"
                                type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            {{ $search['limit'] }} <i class="mdi mdi-chevron-down"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="">
                            @foreach($limits as $limit)
                                <a class="dropdown-item"
                                   href="{{ route('admin.blogs.index', ['q'=>$search['q'], 'sort'=>$search['sort'], 'limit'=>$limit]) }}">{{ $limit }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div>
                    <a href="{{ route('admin.blogs.create') }}" class="btn btn-lg text-primary border border-primary border-0"><i class="fa fa-plus"></i> New Post</a>
                {{-- <small>Page {{ $posts->currentPage() }} of {{ $posts->total() }} results</small> --}}
                </div>
            </div>

            <div class="collapse mt-3 {{ $search['q'] || $search['sort'] ? 'show' : '' }}" id="collapseFilter" style="">
                <form method="GET" action="{{ route('admin.blogs.index') }}">
                    <input type="hidden" name="limit" value="{{ $search['limit'] }}">

                    <div class="row">
                        <div class="form-group col-md-4">
                            <input type="text" class="form-control"
                                   name="q" value="{{ $search['q'] }}"
                                   placeholder="Title..."/>
                        </div>
                        <div class="form-group col-md-3">
                            <select class="form-control" name="sort">
                                <option value="">--Sort--</option>
                                @foreach($sort_list as $sort => $name)
                                    <option value="{{ $sort }}"
                                            @if($sort === $search['sort']) selected @endif>
                                        {!! $name !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2 ">
                            <button type="submit" class="btn btn-light btn-block"><i class="fa fa-search"></i> Search
                            </button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>

    <div class="row">
        @forelse($posts as $post)
            <div class="col-md-4">
                <div class="card post-card">

                    <a href="{{ route('admin.blogs.show', $post->id) }}">
                        <img class="card-img-top img-fluid"
                             src="{{ $post->getImageURL() }}" alt="Card image cap">
                    </a>


                    <div class="card-body">
                        <a href="{{ route('admin.blogs.show', $post->id) }}">
                            <h5 class="card-title overflow-ellipsis" title="{{ $post->title }}">{{ $post->title }}</h5>
                        </a>

                        <div class="card-text d-flex justify-content-between align-items-center mb-2">
                            <small class="text-muted">Uploaded {{ $post->created_at->diffForHumans() }}</small>
                            <span class="d-flex">
                                <a href="{{ route('admin.blogs.edit', $post->id) }}" class="btn btn-sm btn-default text-primary pr-0">Edit</a>
                                <form method="POST" action="{{ route('admin.blogs.destroy', $post->id) }}">
                                    @method('DELETE')
                                    @csrf
                                        <button type="submit" class="btn btn-sm btn-default text-danger pl-1">Delete</button>
                                </form>

                            </span>
                        </div>


                        <p class="card-text">{{ Str::limit($post->description, 100) }}</p>

                    </div>
                </div>
            </div>
        @empty
        @endforelse
    </div>


    {{ $posts->appends(['q'=>$search['q'], 'sort'=>$search['sort'], 'limit'=>$search['limit']])->links() }}
@endsection

@section('footer_script')
    <script></script>
@endsection
