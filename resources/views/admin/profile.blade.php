@extends('admin.layouts.app')
@section('title', 'Edit Settings')

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Edit Settings</li>
                    </ol>
                </div>
                <h4 class="page-title">Edit Settings</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="row">
        <div class="col-sm-6">

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Site Settings</h4>
                    <form method="post" action="{{ route('admin.settings.edit') }}">
                        @csrf

                        <div class="form-group">
                            <label>Site Name <em>*</em></label>
                            <input type="text"
                                   name="site_name" value="{{ old('site_name', $setting[0]->site_name) }}"
                                   class="form-control {{ $errors->has('site_name') ? ' is-invalid' : '' }}"
                                   placeholder="Site Name" required="">

                            @if ($errors->has('site_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('site_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Site Email <em>*</em></label>
                            <input type="email" value="{{ old('site_email', $setting[0]->site_email) }}"
                                   class="form-control {{ $errors->has('site_email') ? ' is-invalid' : '' }}" name="site_email"
                                   placeholder="Email" required >

                            @if ($errors->has('site_email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('site_email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group text-right">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Change Password</h4>
                    <form method="post" action="{{ route('admin.profile.password') }}">
                        @csrf

                        <div class="form-group">
                            <label>Current Password <em>*</em></label>
                            <input type="password"
                                   class="form-control {{ $errors->has('current_password') ? "is-invalid" : "" }}"
                                   name="current_password">

                            @if ($errors->has('current_password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('current_password') }}
                                </div>
                            @endif
                        </div>

                        <!-- New Password -->
                        <div class="form-group">
                            <label>{{ __('New Password') }} <em>*</em></label>
                            <input type="password"
                                   class="form-control {{ $errors->has('password') ? "is-invalid" : "" }}"
                                   name="password">

                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>

                        <!-- Retype New Password -->
                        <div class="form-group">

                            <label>{{ __('Retype New Password') }} <em>*</em></label>

                            <input type="password" class="form-control"
                                   name="password_confirmation">

                        </div>

                        <div class="form-group text-right">
                            <button class="btn btn-primary" type="submit">Change Password</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Basic Info</h4>
                    <form method="post" action="{{ route('admin.profile.edit') }}">
                        @csrf

                        <div class="form-group">
                            <label>Name <em>*</em></label>
                            <input type="text"
                                   name="name" value="{{ old('name', $user->name) }}"
                                   class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                   placeholder="Name" required="">

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" value="{{ $user->email }}"
                                   class="form-control"
                                   placeholder="Email" readonly>

                        </div>

                        <div class="form-group text-right">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('footer_script')
    <script></script>
@endsection
