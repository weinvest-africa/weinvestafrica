@extends('admin.layouts.app')
@section('title', $member->name)

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.team.index') }}">Team</a></li>
                        <li class="breadcrumb-item active">{{ $member->name }}</li>
                    </ol>
                </div>
                <h4 class="page-title">Team member Detail</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row flex-column-reverse flex-md-row">
        <div class="col-md-7">
            <div class="card d-block">
                <div class="card-body">
                    <!-- member title-->
                    <h3 class="mt-0 font-20">
                        {{ $member->name }}
                    </h3>

                    <p class="my-3 text-muted">
                        {!! $member->description !!}
                    </p>

                </div> <!-- end card-body-->

            </div>
        </div>
        <div class="col-md-5">

            <div class="card d-block text-center p-3">
                <img src="{{ $member->getImageURL() }} " class="img-fluid rounded-circle img-thumbnail avatar-xl">
                <div class="card-text my-3">
                    <h3>{{ $member->position }}</h3>
                    <h5>{{ $member->email }}</h5>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_script')
    <script></script>
@endsection
