@extends('admin.layouts.app')
@section('title', 'New Member')

@section('header-style')
    <style>
        .summernote {
            background: transparent;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        .summernote .btn {
            color: rgba(0, 0, 0, .54)!important;
            background-color: transparent!important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        .summernote {
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.team.index') }}">Team</a></li>
                        {{-- <li class="breadcrumb-item active">{{ $member->name }}</li> --}}
                    </ol>
                </div>
                <h4 class="page-title">New Team Member</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">New Team Member</h5>
            <form method="POST" action="{{ route('admin.team.store') }}" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label>Name <em>*</em></label>
                    <input type="text" name="name"
                           value="{{ old('name') }}"
                           class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label>E-mail <em>*</em></label>
                    <input type="text" name="email"
                           value="{{ old('email') }}"
                           class="form-control @error('email') is-invalid @enderror">
                    @error('email')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Role <em>*</em></label>
                    <input type="text" name="position"
                           value="{{ old('position') }}"
                           class="form-control @error('position') is-invalid @enderror">
                    @error('position')
                        <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                 <div class="form-group">
                    <label>Image</label>
                    <input type="file" name="image"
                           value="{{ old('image') }}"
                           class="form-control @error('image') is-invalid @enderror">
                    @error('image')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group summernote">
                    <label>Description</label>
                    <textarea id="member-summernote" class="form-control @error('description') is-invalid @enderror" name="description" rows="5">{{ old('description') }}</textarea>
                    @error('description')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>

@endsection

@section('footer_script')
    <script>
        $('#member-summernote').summernote({
            minHeight: 200,
            placeholder: 'Write here ...',
            focus: false,
            airMode: false,
            fontNames: ['Roboto', 'Calibri', 'Times New Roman', 'Arial'],
            fontNamesIgnoreCheck: ['Roboto', 'Calibri'],
            dialogsInBody: true,
            dialogsFade: true,
            disableDragAndDrop: false,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['style', 'ul', 'ol', 'paragraph']],
                ['fontsize', ['fontsize']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['height', ['height']],
                ['misc', ['undo', 'redo', 'print', 'help', 'fullscreen']]
            ],
            popover: {
                air: [
                ['color', ['color']],
                ['font', ['bold', 'underline', 'clear']]
                ]
            },
            print: {
                //'stylesheetUrl': 'url_of_stylesheet_for_printing'
            }
        });
        $("#image").on('change', () => {
            var image = $("input#image").val()
            $("label#filename").text(basename(image, "\\"))
        })

        function basename(str, sep) {
            return str.substr(str.lastIndexOf(sep) + 1);
        }
    </script>
@endsection
