@extends('admin.layouts.app')
@section('title', 'Team')

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Team</li>
                    </ol>
                </div>
                <h4 class="page-title"> Team</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    {{-- Filter Section --}}
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="{{ $search['q'] || $search['position'] ? 'show' : '' }}" id="collapseFilter" style="">
                            <form class="form-inline" accept="{{ route('admin.team.index') }}">
                                <div class="form-group">
                                    <label for="inputPassword2" class="sr-only">Search</label>
                                    <input type="search" class="form-control" name="q" value="{{ $search['q'] }}" id="inputPassword2" placeholder="Search...">
                                </div>
                                <div class="form-group mx-sm-3">
                                    <select class="form-control" name="sort">
                                        <option value="">--Sort--</option>
                                        @foreach($sort_list as $sort => $name)
                                            <option value="{{ $sort }}"
                                                    @if($sort === $search['sort']) selected @endif>
                                                {!! $name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mr-sm-3">
                                    <select class="form-control" id="position" name="position">
                                        <option value="">--Position--</option>
                                        @foreach(\App\Member::orderBy('position', 'ASC')->get() as $member)
                                            <option value="{{ $member->id }}"
                                                    @if($member->id == $search['position']) selected @endif>
                                                {{ $member->position }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mr-sm-3 ">
                                    <button type="submit" class="btn btn-light btn-block"><i class="fa fa-search"></i> Search
                                    </button>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="text-lg-right mt-3 mt-lg-0">
                            <a href="{{ route('admin.team.create') }}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i> Add New</a>
                        </div>
                    </div><!-- end col-->
                </div> <!-- end row -->
            </div> <!-- end card-box -->
        </div><!-- end col-->
    </div>

    <div class="row">
        @foreach($members as $member)
            <div class="col-lg-4">
                <div class="text-center card-box">
                    <div class="pt-2 pb-2">
                        <a href="{{ route('admin.team.show', $member->id) }}">
                            <img src="{{ $member->getImageURL() }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">
                        </a>

                        <h4 class="mt-3"><a href="{{ route('admin.team.show', $member->id) }}" class="text-dark">{{ $member->name }}</a></h4>
                        <h6>{{ $member->email }}</h6>

                        <p class="text-muted lead">@ {!! $member->position !!}</p>

                        <div class="d-flex justify-content-center">
                            <a href="{{ route('admin.team.edit', $member->id) }}" class="btn btn-success btn-sm waves-effect waves-light">Edit</a>
                         <form method="POST" action="{{ route('admin.team.destroy', $member->id) }}">
                            @method('DELETE')
                            @csrf
                            <button style="margin-left:3px" type="submit" class="btn btn-danger btn-sm waves-effect">Delete</button>
                         </form>
                        </div>

                        <p class="card-text py-4">{{ $member->getShortDesc() }}</p>

                    </div> <!-- end .padding -->
                </div> <!-- end card-box-->
            </div> <!-- end col -->
        @endforeach
    </div>
@endsection
