<div class="left-side-menu">

    <div class="h-100" data-simplebar>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul id="side-menu">

                <li class="menu-title">Navigation</li>

                <li class="{{ Route::is('admin.dashboard') ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.dashboard') }}">
                        <i data-feather="airplay"></i>
                        <span> Dashboards </span>
                    </a>
                </li>

                <li class="menu-title mt-2">Projects</li>

                <li class="{{ (Request::is('admin/projects*') && 'pending' === Request::get('status')) ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.projects.index', ['status' => 'pending']) }}" class="active">
                        <i data-feather="users"></i>
                        <span> Pending Review </span>
                    </a>
                </li>

                <li class="{{ (Request::is('admin/projects*') && 'pending' !== Request::get('status')) ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.projects.index') }}">
                        <i data-feather="layers"></i>
                        <span> List </span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/categories*') ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.categories.index') }}">
                        <i data-feather="book-open"></i>
                        <span> Categories </span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/countries*') ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.countries.index') }}">
                        <i data-feather="globe"></i>
                        <span> Countries </span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/blogs*') ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.blogs.index') }}">
                        <i data-feather="bold"></i>
                        <span> Blogs </span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/cities*') ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.cities.index') }}">
                        <i data-feather="map-pin"></i>
                        <span> Cities </span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/team*') ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.team.index') }}">
                        <i class="mdi mdi-account-multiple"></i>
                        <span> Team </span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/faqs*') ? 'menuitem-active' : '' }}">
                    <a href="{{ route('admin.faqs.index') }}">
                        <i data-feather="help-circle"></i>
                        <span> FAQs </span>
                    </a>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
