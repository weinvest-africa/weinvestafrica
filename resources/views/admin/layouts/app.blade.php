<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8"/>
    <title>@yield('title') | Back-office Administrateur | Mohada</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('admin_assets/images/favicon.png') }}">

    <!-- Plugins css -->
    <link href="{{ asset('admin_assets/libs/selectize/css/selectize.bootstrap3.css') }}" rel="stylesheet"/>

    <!-- App css -->
    <link href="{{ asset('admin_assets/css/summernote-bs4.css') }}" rel="stylesheet">
    <link href="{{ asset('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('admin_assets/css/app.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('admin_assets/css/user.css') }}" rel="stylesheet"/>

    <!-- icons -->
    <link href="{{ asset('admin_assets/css/icons.min.css') }}" rel="stylesheet" type="text/css"/>

    @yield('header-style')
</head>

<body data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": false}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>

<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
@include('admin.layouts.header')
<!-- end Topbar -->


    <!-- ========== Left Sidebar Start ========== -->
@include('admin.layouts.sidebar')
<!-- Left Sidebar End -->


    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                @include('admin.layouts.alerts')

                @yield('content')

            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <script>document.write(new Date().getFullYear())</script> &copy; Mohada Back-office
                        Administrateur
                    </div>
                    <div class="col-md-6">
                        <div class="text-md-right footer-links d-none d-sm-block">
                            <a href="{{ route('index') }}" target="_blank">Home</a>
                            <a href="{{ route('investments') }}" target="_blank">Investments</a>
                            <a href="{{ route('services') }}" target="_blank">Services</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<!-- Vendor js -->
<script src="{{ asset('admin_assets/js/vendor.min.js') }}"></script>

<!-- Plugins js-->
<script src="{{ asset('admin_assets/libs/apexcharts/apexcharts.min.js') }}"></script>

<script src="{{ asset('admin_assets/libs/selectize/js/standalone/selectize.min.js') }}"></script>

<!-- App js-->
<script src="{{ asset('admin_assets/js/app.min.js') }}"></script>

{{-- Summernote --}}
<script src="{{ asset('admin_assets/js/summernote-bs4.min.js') }}"></script>

@yield('footer_script')
</body>
</html>
