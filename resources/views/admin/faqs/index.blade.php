@extends('admin.layouts.app')
@section('title', 'Frequently Asked Questions')

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active">Frequently Asked Questions</li>
                    </ol>
                </div>
                <h4 class="page-title">Frequently Asked Questions</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">FAQs</h5>
                    <div class="table-responsive">
                        <table class="table table-stripped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Questions</td>
                                <td>Answers</td>
                                <td>Action</td>
                            </tr>
                            </thead>
                            <tbody>
                                @forelse($faqs as $faq)
                                    @if(!$faq->voided)
                                        <tr>
                                            <td>{{ $loop->index + 1 }}</td>
                                            <td>{{ $faq->question }}</td>
                                            <td>{{ Str::limit($faq->answer, 10) }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-info btn-sm" title="view more"
                                                            data-toggle="modal" data-target="#view{{ $faq->id }}">
                                                        <i class="fa fa-eye"></i></button>
                                                    <a href="{{ route('admin.faqs.edit', ['faq' => $faq->id]) }}"
                                                    class="btn btn-light btn-sm" title="Edit"><i
                                                                class="fa fa-pen"></i></a>
                                                    <button class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                                                            data-target="#delete{{ $faq->id }}">
                                                        <i class="fa fa-trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>

                                        <div id="delete{{ $faq->id }}" class="modal fade" tabindex="-1" role="dialog"
                                            aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content modal-filled bg-danger">
                                                    <div class="modal-body p-4">
                                                        <form method="POST"
                                                            action="{{ route('admin.faqs.destroy', ['faq' => $faq->id]) }}">
                                                            @method('DELETE')
                                                            @csrf
                                                            <div class="text-center">
                                                                <i class="dripicons-wrong h1 text-white"></i>
                                                                <h4 class="mt-2 text-white">Delete {{ $faq->question }}
                                                                    !</h4>
                                                                <p class="mt-3 text-white">
                                                                    Are you sure you want to delete this faq?
                                                                </p>
                                                                <button type="submit" class="btn btn-light my-2">Proceed
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @else
                                        <tr class="text-muted">
                                            <td class="strike-through">{{ $loop->index + 1 }}</td>
                                            <td class="strike-through">{{ $faq->question }}</td>
                                            <td class="strike-through">{{ Str::limit($faq->answer, 10) }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-info btn-sm" title="view more"
                                                            data-toggle="modal" data-target="#view{{ $faq->id }}">
                                                        <i class="fa fa-eye"></i></button>
                                                    <button class="btn btn-light btn-sm" data-toggle="modal"
                                                            data-target="#restore{{ $faq->id }}">Restore
                                                    </button>
                                                </div>

                                            </td>
                                        </tr>

                                        <div id="restore{{ $faq->id }}" class="modal fade" tabindex="-1" role="dialog"
                                            aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content modal-filled">
                                                    <div class="modal-body p-4">
                                                        <form method="POST"
                                                            action="{{ route('admin.faqs.restore', ['faq' => $faq->id]) }}">
                                                            @method('PATCH')
                                                            @csrf
                                                            <div class="text-center">
                                                                <i class="dripicons-warning h1 text-warning"></i>
                                                                <h4 class="mt-2 ">Restore {{ $faq->question }}
                                                                    !</h4>
                                                                <p class="mt-3">
                                                                    Are you sure you want to restore this faq?
                                                                </p>
                                                                <button type="submit" class="btn btn-warning my-2">Proceed
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endif

                                    <div class="modal fade" id="view{{ $faq->id }}" tabindex="-1" role="dialog"
                                        style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myCenterModalLabel">Details</h4>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">×
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>{{ $faq->question }}</h5>
                                                    <p>{{ $faq->answer ?: 'No Answer' }}</p>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">No Category Found</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        @if(!$search['voided'])
                            <a href="{{ route('admin.faqs.index', ['voided' => true]) }}"
                               class="small text-muted">Show deleted faqs</a>
                        @else
                            <a href="{{ route('admin.faqs.index') }}"
                               class="small text-muted">Hide deleted faqs</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">New FAQ</h5>
                    <form method="POST" action="{{ route('admin.faqs.store') }}">
                        @csrf

                        <div class="form-group">
                            <label>Question <em>*</em></label>
                            <input type="text" name="question"
                                   value="{{ old('question') }}"
                                   class="form-control @error('question') is-invalid @enderror">
                            @error('question')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Answer</label>
                            <textarea class="form-control @error('answer') is-invalid @enderror"
                                      name="answer" rows="5">{{ old('answer') }}</textarea>
                            @error('answer')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer_script')
    <script></script>
@endsection
