<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        .infor {
            padding-top: 1em;
        }
        .mb-0 {

        }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <h2>A Question from {{ $data->name }}</h2>

    <p class="lead text-secondary">
        {{ $data->comment }}
    </p>

    <div id="infor">
        <span>
            <h4  class="mb-0">E-mail:  </h4>{{ $data->email }}
        </span>
        <span>
            <h4 class="mb-0">Phone:  </h4>{{ $data->phone }}
        </span>

    </div>

</body>
</html>
