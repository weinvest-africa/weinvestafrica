<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        .infor {
            padding-top: 1em;
        }
        .mb-0 {

        }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <h2>Mail from WeInvestInAfrica</h2>

    <p class="lead text-secondary">
        {!! $data->investordesc !!}
    </p>

    <div id="infor">
        <span>
            <h4 class="mb-0">Name: </h4> {{ $data->investorname }}
        </span>
        <span>
            <h4  class="mb-0">E-mail:  </h4>{{ $data->investoremail }}
        </span>
        <span>
            <h4 class="mb-0">Phone:  </h4>{{ $data->investorphone }}
        </span>

    </div>

</body>
</html>
