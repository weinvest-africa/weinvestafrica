@extends('layouts.app')

@section('content')
   {{-- Head Image --}}
   <div id="postproject">
       <div class="container">
           <div class="row mt-5">
                <div class="col-md-12">
                    <h1 class="text-center">{{ $title }}</h1>
                </div>
            </div>
       </div>
   </div>

   {{-- Some Random Text --}}
   <div id="desc">
       <div class="container">
           <div class="col-md-12">
               <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h1>
           </div>
       </div>
   </div>

   {{-- The Project form --}}
   <div id="projectform">
       <div class="container">
           <form action="{{ route('projects.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

                <div class="row">
                    <div class="col-md-7">
                        {{-- Categories --}}
                        <div class="form-group">
                            <label for="category">Category</label>

                            <select name="category" id="category" class="form-control @error('category') is-invalid @enderror">
                                <option value="">Choose a Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        @if(old('category') == $category->id) selected @endif>
                                        {{ $category->name }}</option>
                                @endforeach
                            </select>
                            <i class="fa fa-angle-down"></i>

                            @error('category')
                                <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Project name --}}
                        <div class="form-group">
                            <label for="projectname">Project Name</label>
                            <input type="text" name="projectname"
                                value="{{ old('projectname') }}"
                             id="projectname" class="form-control @error('projectname') is-invalid @enderror" placeholder="Enter a project name">

                            @error('projectname')
                                <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Investment Amount --}}
                        <div class="form-group">
                            <label>Investment Amount($)</label>
                            <input type="number" name="investment"
                                   value="{{ old('investment') }}"
                                   min="0"
                                   class="form-control @error('investment') is-invalid @enderror"
                                   placeholder="Enter required investment">

                            @error('investment')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Email address --}}
                        <div class="form-group">
                            <label>E-mail address</label>
                            <input type="email" name="email"
                                   value="{{ old('email') }}"
                                   class="form-control @error('email') is-invalid @enderror"
                                   placeholder="Enter required email">

                            @error('email')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Location --}}
                        <div class="form-group">
                            <label>Location</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="country" id="country" class="form-control @error('country') is-invalid @enderror">
                                        <option selected value="">Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}"
                                                @if(old('country') == $country->id) selected @endif>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    <i class="fa fa-angle-down"></i>

                                    @error('country')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="address" value="{{ old('address') }}" placeholder="State / City" >

                                    @error('address')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        {{-- Duration & ROI --}}
                        <div class="form-group">
                            <label>Duration & ROI</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="duration" id="duration" class="form-control @error('duration') is-invalid @enderror">
                                        <option selected value="">Choose Duration</option>
                                        <option value="1" @if(old('duration') == 1) selected @endif>1 month</option>
                                        <option value="6" @if(old('duration') == 6) selected @endif>6 months</option>
                                        <option value="12" @if(old('duration') == 12) selected @endif>1 year</option>
                                        <option value="36" @if(old('duration') == 36) selected @endif>3 years</option>
                                        {{--<option value="5">Other</option>--}}
                                    </select>
                                    <i class="fa fa-angle-down"></i>

                                    @error('duration')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <select name="roi" id="roi" class="form-control @error('roi')
                                        is-invalid @enderror">
                                        <option selected value="">Choose ROI</option>
                                        @for($i=2; $i<7; $i++)
                                        <option value="{{ $i }}"
                                                @if(old('roi') == $i) selected @endif>{{ $i }}x</option>
                                        @endfor
                                    </select>
                                    <i class="fa fa-angle-down"></i>

                                    @error('roi')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        {{-- Description --}}
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror" placeholder="Describe the project" minlength="500">{{ old('description') }}</textarea>

                            @error('description')
                                <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Terms and condition --}}
                        <div class="form-check float-right">
                            <input type="checkbox" name="terms" class="form-check-input @error('terms')
                                is-invalid @enderror" id="terms" >
                            <label for="terms">I accept the <a class="terms" data-toggle="modal" data-target="#staticBackdrop">terms and conditions.</a></label>

                            @error('terms')
                                <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>

                        <!-- Scrollable modal -->
                        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quas nam laborum est excepturi deserunt sint esse fugiat cum, quod consequuntur labore nostrum repudiandae optio, cupiditate, magnam doloribus. Ullam, rerum!</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro minima molestias deserunt dolorem ullam architecto? Voluptatibus, nam amet aliquid quia, officiis nesciunt laborum cumque commodi suscipit consequuntur deleniti neque. Cum, dolorem? Quaerat doloribus aliquid unde distinctio fugiat et quidem quisquam?</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quas nam laborum est excepturi deserunt sint esse fugiat cum, quod consequuntur labore nostrum repudiandae optio, cupiditate, magnam doloribus. Ullam, rerum!</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro minima molestias deserunt dolorem ullam architecto? Voluptatibus, nam amet aliquid quia, officiis nesciunt laborum cumque commodi suscipit consequuntur deleniti neque. Cum, dolorem? Quaerat doloribus aliquid unde distinctio fugiat et quidem quisquam?</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quas nam laborum est excepturi deserunt sint esse fugiat cum, quod consequuntur labore nostrum repudiandae optio, cupiditate, magnam doloribus. Ullam, rerum!</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro minima molestias deserunt dolorem ullam architecto? Voluptatibus, nam amet aliquid quia, officiis nesciunt laborum cumque commodi suscipit consequuntur deleniti neque. Cum, dolorem? Quaerat doloribus aliquid unde distinctio fugiat et quidem quisquam?</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quas nam laborum est excepturi deserunt sint esse fugiat cum, quod consequuntur labore nostrum repudiandae optio, cupiditate, magnam doloribus. Ullam, rerum!</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro minima molestias deserunt dolorem ullam architecto? Voluptatibus, nam amet aliquid quia, officiis nesciunt laborum cumque commodi suscipit consequuntur deleniti neque. Cum, dolorem? Quaerat doloribus aliquid unde distinctio fugiat et quidem quisquam?</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quas nam laborum est excepturi deserunt sint esse fugiat cum, quod consequuntur labore nostrum repudiandae optio, cupiditate, magnam doloribus. Ullam, rerum!</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro minima molestias deserunt dolorem ullam architecto? Voluptatibus, nam amet aliquid quia, officiis nesciunt laborum cumque commodi suscipit consequuntur deleniti neque. Cum, dolorem? Quaerat doloribus aliquid unde distinctio fugiat et quidem quisquam?</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae quas nam laborum est excepturi deserunt sint esse fugiat cum, quod consequuntur labore nostrum repudiandae optio, cupiditate, magnam doloribus. Ullam, rerum!</p>

                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro minima molestias deserunt dolorem ullam architecto? Voluptatibus, nam amet aliquid quia, officiis nesciunt laborum cumque commodi suscipit consequuntur deleniti neque. Cum, dolorem? Quaerat doloribus aliquid unde distinctio fugiat et quidem quisquam?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button id="acceptTerms" type="button" class="btn btn-primary">Accept</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-5">
                        {{-- Image upload: aided by bootstrapious --}}

                        <!-- Uploaded image area-->
                        <p class="font-italic text-white text-center">The image uploaded will be rendered inside the box below.</p>
                        <div class="image-area "><img id="imageResult" src="#" alt="" class="img-fluid rounded shadow-sm mx-auto d-block"></div>

                        <!-- Upload image input-->
                        <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm">
                            <input id="upload" name="upload" type="file" onchange="readURL(this);" class="form-control border-0">
                            <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file</label>
                            <div class="input-group-append">
                                <label for="upload" class="btn btn-light m-0 rounded-pill px-4"> <i class="fa fa-cloud-upload mr-2 text-muted"></i><small class="text-uppercase font-weight-bold text-muted">Choose file</small></label>
                            </div>
                        </div>
                        {{-- Submit --}}
                        <div class="pt-5 mt-3">
                            <button type="submit" class="btn btn-lg text-white postbtn">Post</button>
                        </div>
                    </div>
                </div>

           </form>
       </div>
   </div>

@endsection

@section('footer-script')
<script>
    $('#acceptTerms').on('click', () => {
        $('#terms').prop('checked', true);
        $('button#acceptTerms').attr('data-dismiss', "modal")
    })
</script>
@endsection
