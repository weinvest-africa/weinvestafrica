@extends('layouts.app')

@section('content')
    <div id="investments">
        {{-- Header section --}}
        <div id="investhead">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <h1 class="text-center text-white">{{ $title }}</h1>
                    </div>
                </div>
           </div>
        </div>

        {{-- Body --}}
        <div class="container">
            <div class="row">
                {{-- All Opportunities --}}
                <div class="col-md-9 pl-0">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <h5>Showing all opportunities</h5>
                        </div>
                    </div>
                    <div class="row ">
                        @foreach ($projects as $project)
                            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 mb-4 ">
                                <div class="card">
                                    <a href="{{ route('investments.show', $project->id) }}"><img src="{{ $project->getImageURL() }}" class="card-img-top" alt="project"></a>

                                    <button type="button" class="btn btn-sm btn-tag">{{ \App\Category::find($project->category_id)->name }}</button>
                                    <p class="px-2 mt-1 overflow-ellipsis">In <strong  title="{{ $project->address }}">{{ $project->address }}</strong></strong></p>
                                    <div class="card-body px-2 pt-0">
                                        <h5 class="card-title text-uppercase">{{ $project->name }}</h5>
                                        <div class="row ">
                                            <div class="col-5">
                                                <h6>Duration: </h6>
                                                <h6>Price: </h6>
                                                <h6>Roi: </h6>
                                                <h6>Date: </h6>
                                            </div>
                                            <div class="col-7">
                                                <h6>{{ $project->duration }} Months</h6>
                                                <h6>$ {{ number_format($project->investment) }}</h6>
                                                <h6>{{ $project->roi }}x</h6>
                                                <h6>{{ date('j M Y', strtotime($project->created_at)) }}</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="card-desc pb-2">
                                            <h5>Description</h5>
                                            <h6 class="overflow-ellipsis">{{ $project->description }}</h6>
                                        </div>

                                        <div class="text-center">
                                            <a href="{{ route('investments.show', $project->id) }}" class="investnow btn btn-primary btn-sm text-white text-uppercase">Invest Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="mt-3 d-flex justify-content-center">
                        {{ $projects->links() }}
                    </div>
                </div>

                {{-- Filters --}}
                <div class="col-md-3 ">

                    <form action="{{ route('search') }}" method="GET">
                        @csrf
                        {{-- Search box --}}
                        <div class="row px-0">
                            <div class="col-md-12 pr-1 form-group">
                                <input type="text" class="form-control" placeholder="Search.." name="search">
                                <button type="submit" class="btn btn-default searchbtn"><i class="fa fa-search text-white"></i></button>
                            </div>
                        </div>
                    </form>

                    {{-- Filter Search --}}
                    <form action="{{ route('filter') }}" method="GET">
                        @csrf
                        <div class="row mx-0 mt-2">
                            <div id="filterSearch" class="col-md-12 py-3 form-group">
                                <h3 class="text-secondary">Filter Search</h3>

                                <select name="country" id="country" class="mt-2 form-control">result
                                    <option value="" selected>Country</option>
                                    @foreach ($countries as $country)
                                        <option value="{{ $country->id }}"
                                            @if($country->id == request()->country)
                                                selected
                                            @endif
                                        >{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                <i class="fa fa-angle-down"></i>

                                <select name="address" id="address" class="mt-2 form-control">
                                    <option selected>State / City</option>
                                </select>
                                <i class="fa fa-angle-down"></i>

                                <!-- ROI Range slider: -->
                                <div id="roi-range">
                                    <div class="d-flex">
                                        <label class="upper">{{ request()->roi ? explode(',', request()->roi)[1] : "10" }} %</label>
                                        <label class="my-0 ml-1 font-italic text-secondary">ROI</label>
                                    </div>
                                    <input type="text" class="span1 form-control" value="{{ request()->roi }}" data-slider-min="-20" data-slider-max="20" data-slider-step="1" data-slider-value="-14" data-slider-orientation="vertical" data-slider-selection="after" data-slider-tooltip="hide" id="roi" name="roi">
                                    <div class="d-flex justify-content-between">
                                        <label class="lower">{{ request()->roi ? explode(',', request()->roi)[0] : "5" }} %</label>
                                        <label class="upper">{{ request()->roi ? explode(',', request()->roi)[1] : "10" }} %</label>
                                    </div>
                                </div>

                                <!-- Duration Range slider: -->
                                <div id="duration-range">
                                    <div class="d-flex">
                                        <label class="upper">{{ request()->duration ? explode(',', request()->duration)[1] : "6" }} Months</label>
                                        <label class="my-0 ml-1 font-italic text-secondary">Duration</label>
                                    </div>
                                    <input type="text" class="span2 form-control" value="{{ old('duration') }}" data-slider-min="-20" data-slider-max="20" data-slider-step="1" data-slider-value="-14" data-slider-orientation="vertical" data-slider-selection="after" data-slider-tooltip="hide" id="duration" name="duration">
                                    <div class="d-flex justify-content-between">
                                        <label class="lower">{{ request()->duration ? explode(',', request()->duration)[0] : "3" }} Months</label>
                                        <label class="upper">{{ request()->duration ? explode(',', request()->duration)[1] : "6" }} Months</label>
                                    </div>
                                </div>

                                {{-- Button --}}
                                <div class="text-center py-3 mt-3">
                                    <button type="submit" class="btn filterbtn text-white font-weight-bold" onclick="{{ route('filter') }}">FILTER</button>
                                </div>
                            </div>
                        </div>
                    </form>

                    {{-- Categories --}}

                    <div id="categorieslist" class="row mt-2 mx-0 py-4">
                        <div class="col-md-12">
                            <h2 class="font-weight-bold text-uppercase">Categories</h2>

                            <ul class="list-group">
                                @foreach ($categories as $category)
                                    <li class="list-group-item {{ (Str::contains(url()->current(), $category->name)) ? 'active2' : '' }}" data-href="{{route('category', array($category->name))}}">{{ $category->name }}</li>
                                @endforeach

                            </ul>
                        </div>
                    </div>

                    <input id='cityUrl' type='hidden' value='{{ request()->address }}' />

                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="country"]').on("change", function () {
                console.log("changed")
                var countryId = $(this).val();
                if (countryId) {
                    $.ajax({
                        url: "/investments/cities/" + countryId,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="address"]').empty();
                            $.each(data, (key, value) => {
                                $('select[name="address"]').append(
                                    '<option value="' + key + '">' + value + "</option>"
                                );
                            });
                        },
                    });
                } else {
                    $('select[name="address"]').empty();
                }
            });

            var cId = $('select[name="country"]').val()
            if (cId) {
                $.ajax({
                    url: "/investments/cities/" + cId,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="address"]').empty();
                        var cityUrl = $('#cityUrl').val();
                        $.each(data, (key, value) => {
                            if (cityUrl === key) {
                                $('select[name="address"]').append(
                                    '<option value="' + key + '" selected>' + value + "</option>"
                                );
                            } else {
                                $('select[name="address"]').append(
                                    '<option value="' + key + '">' + value + "</option>"
                                );
                            }
                        });
                    },
                });
            }
        })
    </script>
@endsection
