@extends('layouts.app')

@section('content')
    <div id="services">
        {{-- Header section --}}
        <div id="servicehead">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <h1 class="text-center text-white">{{ $title }}</h1>
                    </div>
                </div>
            </div>
        </div>
        {{-- Welcome Text --}}
        <div id="desc2">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">We offer services for both <span>Investors</span> and <span>Entrepreneurs</span></h1>
                </div>
            </div>
        </div>

        {{-- Body --}}
        <div id="services-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 px-5">
                        <div class="img-overlay position-relative">
                            <img src="{{ asset('images/entrepreneurs.png') }}" alt="entrepreneurs.png" class="img-fluid">
                            <h3>Entrepreneurs</h3>
                        </div>
                        <div class="text-center mt-3">
                            <p class="lead px-3">We help Entrepreneurs to prepare their projects for investors</p>

                            <a href="{{ route('services-details', 'Entrepreneurs') }}" class="btn readmorebtn lead text-white text-capitalize shadow btn-lg">Read more</a>
                        </div>
                    </div>
                    <div class="col-md-6 px-5">
                        <div class="img-overlay position-relative">
                            <img src="{{ asset('images/investors.png') }}" alt="entrepreneurs.png" class="img-fluid">
                            <h3>Investors</h3>
                        </div>
                        <div class="text-center mt-3">
                            <p class="lead px-3">We help Investors to find the right projects to invest in</p>

                            <a href="{{ route('services-details', 'Investors') }}" class="btn readmorebtn lead text-white text-capitalize shadow btn-lg">Read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
