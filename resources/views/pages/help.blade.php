@extends('layouts.app')

@section('content')
    <div id="help">
       {{-- Header section --}}
       <div id="helphead">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <h1 class="text-center text-white">{{ $title }}</h1>
                    </div>
                </div>
            </div>
        </div>

        {{-- Welcome Text --}}
        <div id="desc3">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="text-center">
                        <h1>Frequently Asked <span>Questions</span></h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis quia minima voluptas at labore ad a molestiae, fugit accusamus doloribus quibusdam distinctio veritatis ea totam numquam doloremque ullam voluptates nulla dolorem dicta eos voluptate iste?</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- Body --}}
        <div id="help-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        {{-- FAQs --}}
                        <div id="accordion" class="myaccordion">
                            @foreach($faqs as $faq)
                                <div class="card">
                                    <div class="card-header" id="heading{{ $faq->id }}">
                                        <h5 class="mb-0">
                                            <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse{{ $faq->id }}" aria-expanded="false" aria-controls="collaps{{ $faq->id }}">{{ $faq->question }}
                                                <span class="fa-stack fa-sm">
                                                    <div class="circle">
                                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                    </div>
                                                </span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapse{{ $faq->id }}" class="collapse" aria-labelledby="heading{{ $faq->id }}" data-parent="#accordion">
                                        <div class="card-body mx-3">
                                            {{ $faq->answer }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="d-flex justify-content-center mb-5 py-3">
                            <button data-total="{{ App\Faq::count() }}" class="btn btn-outline show-more">Show more</button>
                        </div>

                        {{-- Questions --}}
                        <div id="questions" class="border border-secondary p-4">
                            <div class="row text-center px-5 py-3">
                                <div class="col-md-12">
                                    <h2 class="mb-2 font-weight-bold">Have any questions?</h2>
                                    <p class="pb-3 text-secondary lead">If you need any more information on our terms or any other inquiries, leave us a message and we will get back to you in less than no time.</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    {{-- Google Maps --}}
                                    <div class="border border-secondary py-5 text-center mb-4">
                                        <h3 class="py-3">Google Maps</h3>
                                    </div>
                                    {{-- Help Info --}}
                                    <div class="help-info">
                                        <h4>Address</h4>
                                        <p class="mb-0">Lefferts Blvd NY 11420 132-1-132-99</p>
                                        <p>East 100th Street Brooklyn, NY 11236, US 1231</p>
                                    </div>
                                    <div class="help-info">
                                        <h4>Phone</h4>
                                        <p class="mb-0">+00888-777-555-000</p>
                                        <p>+00888-777-555-000</p>
                                    </div>
                                    <div class="help-info">
                                        <h4>Email</h4>
                                        <p>projectfinder@office.com</p>
                                    </div>
                                </div>
                                {{-- Contact Form --}}
                                <div class="col-md-8">
                                    <form id="questionform" action="{{ route('contact') }}">
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" value="{{ old('name') }}" placeholder="Your name" class="form-control @error('name') is-invalid @enderror">

                                            @error('name')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Your business email" class="form-control @error('email') is-invalid @enderror">

                                            @error('email')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <input type="phone" name="phone" value="{{ old('phone') }}" id="phone" placeholder="Phone Number" class="form-control @error('phone') is-invalid @enderror">

                                            @error('phone')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group mt-3">
                                            <textarea name="comment" id="comment" cols="30" rows="10" placeholder="Comment" class="form-control @error('comment') is-invalid @enderror">{{ old('comment') }}</textarea>

                                            @error('comment')
                                                <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn">SEND</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Side content --}}
                    <div class="col-md-4">
                        <form id="helpsearchform" action="{{ route('searchblog') }}" method="GET" class="mb-5">
                            @csrf
                            <div class="form-group d-flex">
                                <input type="text" name="searchterm" value="{{ old('searchterm') }}" id="searchterm" class="form-control" placeholder="Search...">
                                <button type="submit" class="form-control btn btn-primary">Search</button>
                            </div>
                        </form>

                        {{-- Recent Posts --}}
                        <div id="recent-posts" class="mt-3">
                            <div class="">
                                <div class="sideheader1">
                                    <h3 class="px-4 border border-secondary text-center">Recent Posts</h3>
                                </div>

                                <div class="scroll postlist border border-secondary">
                                    <h4 class="mt-5 px-4 mb-1">Latest Posts:</h4>

                                    <ul class="list-group">
                                       @foreach($posts as $post)
                                        <li class="list-group-item py-3">
                                            <a href="{{ route('blog.showblog', $post->id) }}">
                                                <h6>{{ $post->title }}</h6>
                                                <span>
                                                    <img src="{{ asset('images/calendar.png') }}" alt="carlendar.png" class="img-fluid" width="15px">
                                                    <small class="ml-1 text-secondary">{{ date('M, Y', $post->created_at->timestamp) }}</small>
                                                </span>
                                            </a>
                                        </li>
                                       @endforeach
                                    </ul>

                                </div>
                            </div>
                        </div>

                        {{-- Updates --}}
                        <div id="updates" class="mt-5">
                            <div class="sideheader2">
                                <h3 class="px-4 border border-secondary text-center">Get Updates</h3>
                            </div>
                            <div id="mc_embed_signup">
                                <form action="https://gmail.us10.list-manage.com/subscribe/post?u=5e648d79d3c63bf59176842ef&amp;id=58de10ef8e" method="POST"  class="pt-5 border border-secondary" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    @csrf
                                    <div class="form-group px-3">
                                        <input type="email" value="" class="form-control" placeholder="Email" name="EMAIL" class="required email" id="mce-EMAIL">
                                        <input type="text" name="b_5e648d79d3c63bf59176842ef_58de10ef8e" tabindex="-1" value="" style="display:none">

                                    </div>
                                    <div class="text-center mb-4">
                                        <button onClick="javscript: form.submit();" type="button" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        $(document).ready(() => {
            $(".show-more").on('click', () => {
                var currentTotal = $(".card").length;
                $.ajax({
                    url: '/help/show-more',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        skip: currentTotal
                    },
                    beforeSend: () => {
                        $(".show-more").html('Loading...');
                    },
                    success: (response) => {
                        var html = '';
                        $.each(response.data, (index, value) => {
                            //data = response.data[index]
                            console.log("Index: ", value)
                            html += '<div class="card">'
                               html += '<div class="card-header" id="heading' + value.id + '">'
                                    html += '<h5 class="mb-0">'
                                        html += '<button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapse' + value.id + '" aria-expanded="false" aria-controls="collapse' + value.id + '">' + value.question
                                            html += '<span class="fa-stack fa-sm">'
                                                html += '<div class="circle">'
                                                    html += '<i class="fa fa-angle-down" aria-hidden="true"></i>'
                                                    html += '</div>'
                                                html += '</span>'
                                            html += '</button>'
                                        html += '</h5>'
                                    html += '</div>'
                                html += '<div id="collapse' + value.id + '" class="collapse" aria-labelledby="heading' + value.id + '" data-parent="#accordion">'
                                    html += '<div class="card-body mx-3">'
                                        html += value.answer
                                    html += '</div>'
                                html += '</div>'
                            html += '</div>'
                        })
                        $(".myaccordion").append(html)

                        //switch load more when nor further result
                        var currentTotal = $(".card").length
                        var total = parseInt($(".show-more").attr('data-total'));
                        console.log("Current total",currentTotal)
                        console.log("total: ", total)
                        if (currentTotal == total) {
                            $(".show-more").remove()
                        } else {
                            $(".show-more").html("Show More")
                        }
                    }
                })
            })
        })
    </script>

    @if(Session::get('success'))

        <script>
            $('html, body').animate({
                scrollTop: $('#helpsearchform').offset().top
            }, 2000);

            window.setTimeout(function() {
                $('#helpsearchform').next('#helpsearchform').show()
            }, 1500)
        </script>
    @endif
@endsection
