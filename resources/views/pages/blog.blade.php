@extends('layouts.app')

@section('content')
    <div id="blog">
        {{-- Blog Header --}}
        <div id="bloghead">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="text-white">Blog<h1>
                    </div>
                </div>
            </div>
        </div>

        {{-- Content --}}
        <div id="blogcontent">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">

                       @foreach($posts as $post)
                            <div class="card mb-4">
                                <a href="{{ route('blog.showblog', $post->id) }}">
                                    <img src="{{ $post->getImageURL() }}" class="overlay img-fluid w-100 h-25" alt="image">
                                </a>
                                <div class="card-body">
                                    <h2 class="card-title">{{ $post->title }}</h2>
                                    <p class="text-secondary">{!! $post->getShortDesc() !!}</p>
                                </div>

                                <div class="card-footer">
                                    <span class="d-flex align-self-center text-muted">
                                        <i class="fa fa-calendar mr-2 my-auto"></i>
                                        <p class="my-auto">{{ date('M d, Y', $post->created_at->timestamp) }}</p>
                                    </span>
                                </div>
                            </div>
                       @endforeach
                       <div class="mt-3 d-flex justify-content-center">
                        {{ $posts->links() }}
                    </div>

                    </div>
                    <div class="col-md-4">
                        <form action="{{ route('searchblog') }}" class="" method="GET">
                        @csrf
                        <div class="row px-0">
                            <div class="col-md-12 pr-1 form-group">
                                <input type="text" class="form-control" placeholder="Search blog.." name="search">
                                <button type="submit" class="btn btn-default searchbtn"><i class="fa fa-search text-white"></i></button>
                            </div>
                        </div>
                    </form>

                    {{-- Recent Posts --}}
                    <div id="recentposts" class="mt-2">
                        <h3 class="pt-2 pb-2">Latest Posts:</h3>
                        @foreach($latestposts as $recentpost)
                            <div class="py-2">
                                <a href="{{ route('blog.showblog', $recentpost->id) }}" class="h5 text-dark" data-toggle="tooltip"  title="{{ $recentpost->title }}">{{ $recentpost->getShortTitle() }}</a>
                                <small class="d-flex align-self-center text-muted mt-1">
                                    <i class="fa fa-calendar pt-1"></i>
                                    <p class="pl-1">{{ date('M d, Y', $recentpost->created_at->timestamp) }}</p>
                                </small>
                                <hr class="my-1">
                            </div>

                        @endforeach
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
