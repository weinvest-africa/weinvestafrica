@extends('layouts.app')

@section('content')
    <div id="future" >
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-9 col-lg-8 col-xl-8 px-md-auto">
                    <div class="darkbg p-5">
                        <h1>Africa is the <span class="font-weight-bolder">future</span></h1>
                        <h2 class="pb-2 mb-4">We help you invest in the future today</h2>
                        <button class="btn btn-primary btn-md font-weight-bold text-white">Get Started</button>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-3 col-lg-4 col-xl-4"></div>
            </div>
        </div>
    </div>

    <div id="steps">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="text-center">
                        <div class="circle">
                            <div class="circle2">
                                <img src="{{ asset('images/search.png') }}" width="40px" class="img-fluid" alt="search">
                            </div>
                        </div>
                        <h3 class="font-weight-bold">Step 1:</h3>
                        <p class="px-4 lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt at amet voluptas libero quisquam adipisci.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="text-center">
                        <div class="circle">
                            <div class="circle2">
                                <img src="{{ asset('images/calendar.png') }}" width="30px" class="img-fluid" alt="calendar" style="margin: 0.5em;padding:0.1em;">
                            </div>
                        </div>
                        <h3 class="font-weight-bold">Step 2:</h3>
                        <p class="px-4 lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt at amet voluptas libero quisquam adipisci.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="text-center">
                        <div class="circle">
                            <div class="circle2">
                                <img src="{{ asset('images/money.png') }}" width="30px" class="img-fluid" alt="payment" style="margin: 0.5em;">
                            </div>
                        </div>
                        <h3 class="font-weight-bold">Step 3:</h3>
                        <p class="px-4 lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt at amet voluptas libero quisquam adipisci.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="opportunities">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="mb-3">Latest <span>Opportunities</span></h1>
                </div>
            </div>
        </div>
    </div>

    <div id="projects" >
        <div class="container">
            <div class="row">
                @foreach ($projects as $project)
                    <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3 mb-4 mx-auto">
                        <div class="card mb-4 mx-auto ">
                            <button type="button" class="btn btn-sm btn-tag">{{ \App\Category::find($project->category_id)->name}}</button>
                            <a href="{{ route('investments.show', $project->id) }}">
                                <img src="{{ $project->getImageURL() }}" class="card-img-top" alt="project">
                            </a>


                            <p class="px-2 mt-1 overflow-ellipsis">In <strong  title="{{ $project->address }}">{{ $project->address }}</strong></strong></p>
                            <div class="card-body px-2 pt-0">
                                <h5 class="card-title text-uppercase">{{ $project->name }}</h5>
                                <div class="row ">
                                    <div class="col-5">
                                        <h6>Duration: </h6>
                                        <h6>Price: </h6>
                                        <h6>Roi: </h6>
                                        <h6>Date: </h6>
                                    </div>
                                    <div class="col-7">
                                        <h6>{{ $project->duration }} Months</h6>
                                        <h6>$ {{ number_format($project->investment) }}</h6>
                                        <h6>{{ $project->roi }}x</h6>
                                        <h6>{{ date('j M Y', strtotime($project->created_at)) }}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer px-2">
                                <h5>Description</h5>
                                <div class="card-desc">
                                    <h6 class="mt-3 overflow-ellipsis">{{ $project->description }}</h6>
                                    {{-- <a href="#" class="readmore btn btn-primary btn-sm text-white text-uppercase">Read More</a> --}}
                                </div>

                                <div class="text-center">
                                    <a href="{{ route('investments.show', $project->id) }}" class="readmore btn btn-primary btn-sm text-white text-uppercase">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

    <div id="investment">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <img src="{{ asset('images/project.jpg') }}" class="img-fluid" height="100px" alt="invest project image">
                </div>
                <div class="col-12 col-md-6 col-lg-6 mt-3">
                    <h2 class="mb-5"><span>Are</span>  you looking for  <span>investment</span>?</h1>
                    <p class="mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil quisquam saepe officiis, aliquam aspernatur pariatur ipsum quaerat cum quo nobis expedita laudantium, dolorum ipsa libero.</p>
                    <button type="submit" class="btn btn-lg rounded text-white">Submit your project</button>
                </div>
            </div>
        </div>
    </div>

    <div id="operation">
        <div class="container-fluid px-0">
            <div class="overlay">
                <h1>How We <span class="font-weight-bold">Operate</span>?</h1>
            </div>
            {{-- <video id="video" width="100%" height="792">
                <source src="{{ asset('videos/buisness-ethics.mp4') }}" type="video/mp4">
            </video> --}}
            {{-- <img id="video" src="{{ asset('images/home-cut.png') }}" class="img-fluid" width="100%" alt="home-cut"> --}}
            <a onclick="playPause()" class="btn">
                <span class="play">
                    <span></span>
                </span>
            </a>
        </div>
    </div>

    <div id="partners">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-md-2">
                    <img src="{{ asset('images/cta.png') }}" alt="cta-icon" class="img-fluid">
                </div>
                <div class="col-md-2">
                    <img src="{{ asset('images/bird.png') }}" alt="bird-icon" class="img-fluid">
                </div>
                <div class="col-md-2">
                    <img src="{{ asset('images/winner.png') }}" alt="winner-icon" class="img-fluid">
                </div>
                <div class="col-md-2">
                    <img src="{{ asset('images/techpoint.png') }}" alt="techpoint-icon" class="img-fluid">
                </div>
                <div class="col-md-2">
                    <img src="{{ asset('images/africa-innovation.png') }}" alt="africa-icon" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <div id="newsletter">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div id="join">
                        <h2>Join Our Newsletter</h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur quo, vero consectetur ut officia alias.</p>
                    </div>
                    <div class="row pt-3">
                        <div class="col-md-8 pr-1">
                            <div class="input-group">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <input type="email" class="form-control" placeholder="You E-mail address">
                            </div>
                        </div>
                        <div class="col-md-4 pl-0">
                            <button class=" text-white subscribe">Subscribe</button>
                        </div>
                    </div>
                    <div class="row ">
                        {{-- <div class="col-md-12 justify-content-right"> --}}
                            <div class="form-check mt-1">
                                <input id="accept" class="form-check-input" type="checkbox" name="accept" value="accepted">
                                <label for="accept" class="form-check-label">I accept the <a href="#">terms of conditions</a></label>
                            </div>
                        {{-- </div> --}}
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>

@endsection
