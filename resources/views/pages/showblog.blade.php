@extends('layouts.app')

@section('content')
    {{-- Blog Header
    <div id="bloghead">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="text-white">Blog<h1>
                </div>
            </div>
        </div>
    </div> --}}

    <div id="blogdetails" class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    {{-- Head --}}
                    <div id="blogdetailhead" class="mb-3">
                        <h1>{{ $post->title }}</h1>
                        <h5 class="pt-2 pb-3 text-muted">{{ date('M d, Y', $post->created_at->timestamp) }}</h5>
                    </div>

                    {{-- Content --}}
                    <div class="">
                        <img src="{{ $post->getImageURL() }}" alt="blog image" class="img-fluid w-100">
                        <div class="">
                            <p class="my-4 card-text text-justify">{!! html_entity_decode($post->description) !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mt-5 pt-5">
                    <form action="{{ route('searchblog') }}" class="mt-3" method="GET">
                        @csrf
                        <div class="row px-0">
                            <div class="col-md-12 pr-1 form-group">
                                <input type="text" class="form-control" placeholder="Search blog.." name="search">
                                <button type="submit" class="btn btn-default searchbtn"><i class="fa fa-search text-white"></i></button>
                            </div>
                        </div>
                    </form>

                    {{-- Recent Posts --}}
                    <div id="recentposts">
                        <h3 class="pt-2 pb-2">Latest Posts:</h3>
                        @foreach($posts as $recentpost)
                            <div class="py-2">
                                <a href="{{ route('blog.showblog', $recentpost->id) }}" class="h5 text-dark" data-toggle="tooltip"  title="{{ $recentpost->title }}">{{ $recentpost->getShortTitle() }}</a>
                                <small class="d-flex align-self-center text-muted mt-1">
                                    <i class="fa fa-calendar pt-1"></i>
                                    <p class="pl-1">{{ date('M d, Y', $recentpost->created_at->timestamp) }}</p>
                                </small>
                                <hr class="my-1">
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
