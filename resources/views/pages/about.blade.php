@extends('layouts.app')

@section('header-style')
    <style>
        .carousel-control-prev-icon,
        .carousel-control-next-icon {
            height: 60px;
            width: 60px;
            background-size: 50%, 50%;
        }

        .carousel-control-next-icon {
            background-image: url(../images/right.png)
        }

        .carousel-control-prev-icon {
            background-image: url(../images/left.png)
        }

        .carousel-control-next-icon:after
        {
            content: '';
            font-size: 55px;
            color: red;
        }

        .carousel-control-prev-icon:after {
            content: '';
            font-size: 55px;
            color: red;
        }
    </style>
@endsection

@section('content')
    <div id="about">
        {{-- About Header --}}
        <div id="abouthead">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="text-white">{{ $title }}</h1>
                    </div>
                </div>
            </div>
        </div>

        {{-- About Content --}}
        <div id="about-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 py-5">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus sed enim consectetur numquam error officiis? Corrupti molestias minus quod blanditiis cum sequi optio ea numquam nesciunt maiores odio, quibusdam dolorum..</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis quisquam similique quia reprehenderit hic optio voluptas adipisci soluta aut eligendi.</p>
                    </div>
                    <div class="col-md-6 py-5">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident, dolorum consectetur! Excepturi hic aspernatur impedit ipsam vel iste voluptatibus. Accusamus, suscipit esse temporibus tempore veritatis quo dolor recusandae tenetur iste adipisci vero impedit distinctio officia!</p>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officia quae obcaecati aliquid incidunt exercitationem dolorum placeat explicabo, ex sint deleniti.</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- Team banner --}}
        <div id="team-banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-white text-center">Team</h2>
                    </div>
                </div>
            </div>
        </div>

        {{-- Team text --}}
        <div id="team-text">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 py-3">
                        <p class="text-center mt-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel ullam suscipit consequatur deserunt omnis ipsum recusandae eius labore soluta explicabo tenetur, maiores, aut doloribus. Voluptas.</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- Team Members --}}
        <div id="team-members">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="teamCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach(array_chunk($members->all(), 3) as $memberCollection)
                                    <div class="carousel-item
                                        @if ($loop->first) active @endif
                                    ">
                                        <div class="row">
                                            @foreach($memberCollection as $member)
                                                <div class="col-md-4">
                                                    <div class="py-5 px-3 border border-secondary">
                                                        <div class="py-3 text-center">
                                                            <img src="{{ $member->getImageURL() }}" alt="picture" class="img-fluid rounded-circle">
                                                        </div>
                                                        <div class="text-center py-3">
                                                            <h4>{{ $member->name }}</h4>
                                                            <h5>{{ $member->position }}</h5>
                                                        </div>
                                                        <div class="pt-3 text-center">
                                                            <p class=" mb-0 text-secondary">{{ $member->description }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#teamCarousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#teamCarousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
