@extends('layouts.app')

@section('content')
    <div id="services">
        {{-- Header section --}}
        <div id="servicehead">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-md-12">
                        <h1 class="text-center text-white">Services for {{ request()->name }}</h1>
                    </div>
                </div>
            </div>
        </div>

        {{-- Some Random Text --}}
        <div id="services-desc">
            <div class="row">
                <div class="col-md-12">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h1>
                </div>
            </div>
        </div>

        {{-- Body --}}
        <div id="services-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Consulting</h3>
                        <p class="lead">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Neque natus nihil tempore consequuntur? Optio autem aliquid, qui architecto voluptatum minima cupiditate eum iste impedit soluta atque est obcaecati iusto tenetur nostrum doloribus ab a quod sequi, voluptates magni sapiente. Quisquam ullam, nisi optio quis laboriosam accusantium, est enim voluptatibus voluptatum eos quo at dicta officiis sequi ex? Mollitia dolor provident iusto molestias, ipsa suscipit labore.</p>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed expedita nisi quibusdam vel omnis sapiente accusamus atque at, blanditiis reprehenderit veniam libero nobis possimus voluptas mollitia aperiam nostrum corporis totam quidem optio laboriosam quia minima rem repudiandae? Dolorem, nostrum labore?</p>
                        <h3>Accounting</h3>
                        <p class="lead">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Neque natus nihil tempore consequuntur? Optio autem aliquid, qui architecto voluptatum minima cupiditate eum iste impedit soluta atque est obcaecati iusto tenetur nostrum doloribus ab a quod sequi, voluptates magni sapiente. Quisquam ullam, nisi optio quis laboriosam accusantium, est enim voluptatibus voluptatum eos quo at dicta officiis sequi ex? Mollitia dolor provident iusto molestias, ipsa suscipit labore.</p>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed expedita nisi quibusdam vel omnis sapiente accusamus atque at, blanditiis reprehenderit veniam libero nobis possimus voluptas mollitia aperiam nostrum corporis totam quidem optio laboriosam quia minima rem repudiandae? Dolorem, nostrum labore?</p>
                    </div>
                    <div class="col-md-6">
                        <h3>Computing</h3>
                        <p class="lead">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Neque natus nihil tempore consequuntur? Optio autem aliquid, qui architecto voluptatum minima cupiditate eum iste impedit soluta atque est obcaecati iusto tenetur nostrum doloribus ab a quod sequi, voluptates magni sapiente. Quisquam ullam, nisi optio quis laboriosam accusantium, est enim voluptatibus voluptatum eos quo at dicta officiis sequi ex? Mollitia dolor provident iusto molestias, ipsa suscipit labore.</p>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed expedita nisi quibusdam vel omnis sapiente accusamus atque at, blanditiis reprehenderit veniam libero nobis possimus voluptas mollitia aperiam nostrum corporis totam quidem optio laboriosam quia minima rem repudiandae? Dolorem, nostrum labore?</p>
                        <h3>Accounting</h3>
                        <p class="lead">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Neque natus nihil tempore consequuntur? Optio autem aliquid, qui architecto voluptatum minima cupiditate eum iste impedit soluta atque est obcaecati iusto tenetur nostrum doloribus ab a quod sequi, voluptates magni sapiente. Quisquam ullam, nisi optio quis laboriosam accusantium, est enim voluptatibus voluptatum eos quo at dicta officiis sequi ex? Mollitia dolor provident iusto molestias, ipsa suscipit labore.</p>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed expedita nisi quibusdam vel omnis sapiente accusamus atque at, blanditiis reprehenderit veniam libero nobis possimus voluptas mollitia aperiam nostrum corporis totam quidem optio laboriosam quia minima rem repudiandae? Dolorem, nostrum labore?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
