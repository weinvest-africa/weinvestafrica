<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Country::count() > 0) {
            return;
        }

        $countries = [
            'Cameroon', 'Congo', 'Tchad', 'Tunis', 'Turky', 'Egypt', 'South Africa'
        ];

        foreach ($countries as $country) {
            $ctry = new Country();
            $ctry->name = $country;
            $ctry->save();
        }
    }
}