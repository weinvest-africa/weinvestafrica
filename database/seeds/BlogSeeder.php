<?php

use App\Blog;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Blog::count() > 0) {
            return;
        }

        $faker = \Faker\Factory::create();
        $storage_path = storage_path('app/public/uploads/posts');
        \File::isDirectory($storage_path) or \File::makeDirectory($storage_path, 0777, true, true);

        for ($i = 0; $i < 20; $i++) {
            $post = new Blog();
            $post->title = $faker->sentence(5);
            $post->post_image = $faker->file(public_path('samples/images/' . mt_rand(5, 11)), $storage_path, false);
            $post->description = $faker->text(400);
            $post->save();
        }
    }
}