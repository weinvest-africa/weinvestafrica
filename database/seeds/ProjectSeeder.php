<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\App\Project::count() > 0) {
            return;
        }

        $faker = Faker\Factory::create();
        $duration = [1, 3, 4, 6, 12, 24];
        $storage_path = storage_path('app/public/uploads/projects');
        \File::isDirectory($storage_path) or \File::makeDirectory($storage_path, 0777, true, true);

        for ($i = 0; $i < 100; $i++) {
            $project = new \App\Project();
            $project->category_id = \App\Category::inRandomOrder()->first()->id;
            $project->name = $faker->text(40);
            $project->email = $faker->email;
            $project->investment = mt_rand(1, 100) * 500;
            $project->country_id = \App\Country::inRandomOrder()->first()->id;
            $project->city_id = \App\City::inRandomOrder()->first()->id;
            $project->address = $faker->address;
            $project->duration = $duration[mt_rand(0, count($duration) - 1)];
            $project->roi = mt_rand(2, 6);
            $project->description = $faker->text(400);
            $project->status = 'approved';
            $project->project_image = $faker->file(public_path('samples/images/' . mt_rand(1, 11)), $storage_path, false);
            $project->save();
        }
    }
}
