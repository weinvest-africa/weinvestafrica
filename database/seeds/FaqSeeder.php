<?php

use Illuminate\Database\Seeder;
use App\Faq;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Faq::count() > 0) {
            return;
        }

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $faq = new Faq();
            $faq->question = Str::replaceLast('.', '?', $faker->sentence(7));
            $faq->answer = $faker->paragraph;
            $faq->save();
        }
    }
}