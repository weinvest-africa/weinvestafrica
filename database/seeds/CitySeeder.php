<?php

use Illuminate\Database\Seeder;
use App\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (City::count() > 0) {
            return;
        }

        $cities = [
            'Yaounde', 'Douala', 'Buea', 'Dschang', 'Lagos', 'Abudja', 'Cape Town'
        ];

        foreach ($cities as $city) {
            $cty = new City();
            $cty->name = $city;
            $cty->country_id = \App\Country::inRandomOrder()->first()->id;
            $cty->save();
        }
    }
}
